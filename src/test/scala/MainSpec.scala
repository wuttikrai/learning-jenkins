package app

import org.specs2.mutable.Specification

class MainSpec extends Specification {
  "new test" >> {
    Main.get must_== "100"
  }

  "new test 2" >> {
    Main.get must_== "100"
  }

  "new test 3" >> {
    Main.get must_== "100"
  }

  "new test 4" >> {
    Main.get must_== "100"
  }

  "new test 5" >> {
    Main.get must_== "100"
  }

  "new test 6" >> {
    Main.get must_== "100"
  }

  "new test 7" >> {
    Main.get must_== "100"
  }

  "new test 8" >> {
    Main.get must_== "100"
  }

  "new test 9" >> {
    Main.get must_== "100"
  }

  "new test 10" >> {
    Main.get must_== "100"
  }
}
